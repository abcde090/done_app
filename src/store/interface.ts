export interface Done extends Object {
  title: string
  goal: string
  datetime: Date
  tags: Set<string>
}

export interface DoneList {
  tags: Set<string>
  list: Array<Done>
}
