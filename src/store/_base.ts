import { ref, Ref, reactive, readonly, watch } from 'vue'
import { set, get } from 'idb-keyval'
import { cloneDeep } from 'lodash-es'

export abstract class Store<T extends Object> {
  protected state: T

  constructor(readonly storeName: string) {
    let data = this.data()
    this.state = reactive(data) as T
  }

  protected abstract data(): T

  public getState(): T {
    return readonly(this.state) as T
  }
}

export abstract class PersistentStore<T extends Object> extends Store<T> {
  private isInitialized = ref(false)

  constructor(readonly storeName: string) {
    super(storeName)
  }

  async init() {
    if (this.isInitialized) {
      // if not already setup, create db
      let stateFromDB: object = await get(this.storeName)

      // if the db existed, hydrate the state from the db
      if (stateFromDB) {
        Object.assign(this.state, stateFromDB)
      }
      // create watch of state to keep the store updated
      watch(
        () => this.state,
        (val) => set(this.storeName, cloneDeep(val)),
        { deep: true }
      )

      this.isInitialized.value = true
    }
  }

  getIsInitialized(): Ref<boolean> {
    return this.isInitialized
  }
}
