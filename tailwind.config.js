const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  purge: {
    enabled: process.env.NODE_ENV === 'production',
    content: ['./src/**/*.vue', './src/**/*.html'],
  },
  theme: {
    fontFamily: {
      body: ['Fantasque Sans Mono', ...defaultTheme.fontFamily.sans],
      display: ['NEON', ...defaultTheme.fontFamily.serif],
      title: ['Cabin Sketch', ...defaultTheme.fontFamily.serif],
    },
    colors: {
      white: '#fff',
      gray: defaultTheme.colors.gray,
      grey: defaultTheme.colors.gray,
      primary: {
        '100': '#4fc08d',
        '200': '#45b990',
        '300': '#42aa90',
        '400': '#3e9c8e',
        '500': '#3b8e89',
        '600': '#377c80',
        '700': '#336972',
        '800': '#2f5865',
        '900': '#2a4858',
      },
      secondary: {
        '100': '#4f82c0',
        '200': '#467bb3',
        '300': '#3e74a5',
        '400': '#386d98',
        '500': '#33668b',
        '600': '#2f5e7d',
        '700': '#2d5771',
        '800': '#2b4f64',
        '900': '#2a4858',
      },
    },
  },
  variants: {},
  plugins: [],
}
